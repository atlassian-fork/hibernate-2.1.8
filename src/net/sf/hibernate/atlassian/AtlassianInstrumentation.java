package net.sf.hibernate.atlassian;

import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * A mechanism to allow Hibernate instrumentation data to be recorded using the host Atlassian products infrastructure.
 * By default no data is recorded. The host product needs to call
 * {@link #registerFactory(net.sf.hibernate.atlassian.AtlassianInstrumentation.AtlasSplitFactory)} to register a
 * factory that will create {@link net.sf.hibernate.atlassian.AtlassianInstrumentation.AtlasSplit} instances. Factory
 * can be unregistered with a call to {@link #unregisterFactory(AtlasSplitFactory)}. Multiple factories are supported.
 */
public class AtlassianInstrumentation
{
    /** Represents a timer split. */
    public interface AtlasSplit
    {
        /** Stops the timer split and records the result. Should only be called once. */
        void stop();
    }

    /**
     * Represents a factory for creating {@link net.sf.hibernate.atlassian.AtlassianInstrumentation.AtlasSplit}.
     */
    public interface AtlasSplitFactory
    {
        /**
         * Creates a split associated with a timer of a specified name.
         * @param name the name of the timer to associate the split time with.
         * @return a started split.
         */
        AtlasSplit startSplit(String name);
    }

    private static class NopAtlasSplit implements AtlasSplit
    {
        private static final NopAtlasSplit INSTANCE = new NopAtlasSplit();
        @Override
        public void stop()
        {
        }
    }

    // Any object could be used as a lock, but using a separate object makes
    // the scope of this lock obvious.
    private static final Object modificationLock = new Object();

    private static final Collection<AtlasSplitFactory> atlasSplitFactories = new CopyOnWriteArrayList<AtlasSplitFactory>();

    /**
     * Register a factory for use.
     * @param factory the factory for use. If <tt>null</tt>, then no factory is registered.
     */
    public static void registerFactory(AtlasSplitFactory factory)
    {
        synchronized (modificationLock) // Ensure the same factory cannot be added multiple times
        {
            if (factory != null && !atlasSplitFactories.contains(factory))
            {
                atlasSplitFactories.add(factory);
            }
        }
    }

    /**
     * Unregister a factory.
     * @param factory the factory to unregister. If <tt>null</tt>, then no factory is unregistered.
     */
    public static void unregisterFactory(AtlasSplitFactory factory)
    {
        atlasSplitFactories.remove(factory);
    }

    /**
     * Create a started split, using the registered factory. If no factory is registered, then a <i>NOP</i>
     * instance is returned instead.
     * @param name the name of the timer to associate the split time with.
     * @return an instance for use. Will never be <tt>null</tt>.
     */
    public static AtlasSplit startSplit(String name)
    {
        final int splitFactoryCount = atlasSplitFactories.size();
        if (splitFactoryCount == 0)
        {
            return NopAtlasSplit.INSTANCE;
        }
        final Collection<AtlasSplit> splits = new ArrayList<AtlasSplit>(splitFactoryCount);
        for (AtlasSplitFactory atlasSplitFactory : atlasSplitFactories)
        {
            splits.add(atlasSplitFactory.startSplit(name));
        }
        return new CombinedSplit(splits);
    }

    private static class CombinedSplit implements AtlasSplit
    {
        private final Iterable<AtlasSplit> splits;

        private CombinedSplit(Iterable<AtlasSplit> splits)
        {
            this.splits = splits;
        }

        @Override
        public void stop()
        {
            for (AtlasSplit split : splits)
            {
                split.stop();
            }
        }
    }
}
